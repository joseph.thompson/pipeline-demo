import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('<App />', () => {
  test('renders app without crashing', () => {
    render(<App />);
    const app = screen.getByText(/commit code/i);
    expect(app).toBeInTheDocument();
  });
});
