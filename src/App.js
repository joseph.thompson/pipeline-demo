import React from 'react';

function App() {
  return (
    <div className="App">
      <h1>Commit Code changes for pipeline build.</h1>
    </div>
  );
}

export default App;
